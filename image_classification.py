import keras
import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K

import data_split
import labels_to_int
import numpy as np

img_width, img_height = 150, 150
epochs = 50
batch_size = 16

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

#Model Shape
model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

#data setup
def get_data_x_y(dataset):
    data = []
    labels = []
    for cls in dataset:
        for image in cls.image_paths:
            data.append(image)
            labels.append(cls.name)

    data = np.asarray(data)
    labels = np.asarray(labels)

    return data, labels

#get the training set, test set and the number of classes
train_set, test_set, num_classes = data_split.get_train_test_set()
#extract data(x) and labels(y) for the training set
train_x, train_y = get_data_x_y(train_set)
#extract data(x) and labels(y) for the test set
test_x, test_y = get_data_x_y(test_set)

#convert training labels to ints and provide lookup table to get correct labels for test set
train_y_int, int_label_lookup_dict = labels_to_int.labels_to_int(train_y)
#convert test labels to ints
test_y_int = labels_to_int.int_label_lookup(test_y, int_label_lookup_dict)

#convert labels from ints to categorical
train_y_cat = keras.utils.to_categorical(train_y_int, num_classes)
test_y_cat = keras.utils.to_categorical(test_y_int, num_classes)

#reshape x data 

model.compile(loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy'])

model.fit(train_x, train_y_cat,
        batch_size=batch_size,
        epochs=epochs,
        verbose=1,
        validation_data = (test_x, test_y_cat))

score = model.evaluate(test_x, test_y, verbose = 0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

model.save_weights('face_try.h5')
