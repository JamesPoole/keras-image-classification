import numpy as np

"""
labels_to_int - function to convert labels to ints because svm will not accept strings as labels

args    labels - array of labels to be converted

returns int_labels - array of labels in int form 
        int_label_dict - dictionary for easy lookup of name to int
"""
def labels_to_int(labels):
    current_name = ""
    current_int = 0
    int_labels = []
    int_label_dict ={}
    for name in labels:
        if name != current_name:
            current_name = name
            int_labels.append(current_int)
            int_label_dict.update({current_name: current_int})
            current_int += 1
        else:
            int_labels.append(current_int)
    
    int_labels = np.asarray(int_labels)

    #returns the label array in ints and a dictionary to easily look up what number belongs to what person
    return int_labels, int_label_dict

"""
int_label_lookup -  function to convert test labels to ints.
                    when given a test set, the data is randomised for fairness
                    but we need to make sure to match the correct labels to the correct ints.

args    test_labels - labels for use in test
        int_label_dict - dictionary for easy label int lookup

return  int_labels - test labels converted to ints
"""
def int_label_lookup(test_labels, int_label_dict):
    int_labels = []
    for name in test_labels:
        int_labels.append(int_label_dict[name])  

    return int_labels


